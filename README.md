# Git Mão na  Massa

### [GIT E CONTROLE DE VERSÃO | GIT MÃO NA MASSA](https://www.youtube.com/watch?v=DoQ0HW0OtA0&list=PLMFPOLE2cW1xAlr3QOM8xV-IBFjzXnsqf)

Controle de versão é um sistema que permite rastrear e gerenciar alterações feitas em arquivos ao longo do tempo. Ele é especialmente útil quando várias pessoas trabalham juntas em um mesmo projeto, pois ajuda a evitar conflitos e permite que as mudanças sejam gerenciadas de forma organizada. Além disso, é uma ferramenta essencial para manter um histórico completo das alterações realizadas em um projeto, possibilitando voltar a versões anteriores caso necessário.

Existem dois tipos principais de controle de versão: sistemas centralizados e sistemas distribuídos. O Git é um exemplo de sistema de controle de versão distribuído, que se tornou amplamente popular e é amplamente utilizado na indústria de desenvolvimento de software.

Agora, vamos falar sobre o Git.

O Git é um sistema de controle de versão distribuído, desenvolvido inicialmente por Linus Torvalds em 2005, o mesmo criador do kernel Linux. Ele foi projetado com foco em velocidade, eficiência e suporte a projetos de todos os tamanhos.

As principais características do Git incluem:

1. **Distribuído**: Cada desenvolvedor que trabalha com um repositório Git tem uma cópia completa do histórico do projeto, incluindo todas as versões e alterações. Isso significa que os desenvolvedores podem trabalhar offline e depois sincronizar suas alterações quando estiverem conectados.
2. **Rapidez e eficiência**: O Git foi projetado para ser rápido, tornando operações como commit, branch e merge bastante ágeis, mesmo em projetos grandes com muitos arquivos.
3. **Integridade dos dados**: Cada versão do projeto é identificada por um hash SHA-1, garantindo que os arquivos e histórico não sejam corrompidos.
4. **Suporte a branches**: O Git torna fácil a criação e gestão de branches, permitindo que diferentes recursos ou correções sejam desenvolvidos separadamente e depois integrados ao projeto principal.
5. **Recuperação e histórico**: O Git mantém um histórico completo de todas as alterações realizadas no projeto, o que facilita a recuperação de versões anteriores ou a identificação de quando e por quem uma mudança específica foi feita.
6. **Compatibilidade**: O Git é compatível com uma variedade de serviços e plataformas de hospedagem, como GitHub, GitLab e Bitbucket, o que o torna uma escolha popular para projetos colaborativos.

Para usar o Git no dia a dia, é importante aprender os comandos básicos, como `git init` (para iniciar um repositório Git), `git add` (para adicionar arquivos às áreas de staging), `git commit` (para confirmar as alterações), `git push` (para enviar as alterações para um repositório remoto) e `git pull` (para atualizar o repositório local com as alterações remotas).

Além disso, é fundamental entender o fluxo de trabalho do Git, o que envolve a criação e gestão de branches, resolução de conflitos, fusão de branches e a colaboração em projetos compartilhados com outros desenvolvedores.

Existem muitos recursos e tutoriais disponíveis online para aprender Git, incluindo a documentação oficial do Git e cursos em plataformas de ensino online. Praticar regularmente e trabalhar em projetos reais ajudará você a se familiarizar e se tornar mais proficiente no uso do Git no dia a dia.

### [COMEÇANDO A TRABALHAR COM GIT | GIT MÃO NA MASSA](https://www.youtube.com/watch?v=tT0fmuzvKJ4&list=PLMFPOLE2cW1xAlr3QOM8xV-IBFjzXnsqf&index=2)

Vamos aprender os comandos essenciais para começar a trabalhar com o Git no seu dia a dia.

1. **Inicializar um repositório**:

Para começar a usar o Git em um projeto existente ou criar um novo repositório, você precisa inicializá-lo. Isso cria um diretório .git que armazenará todo o controle de versão do seu projeto.

```
git init
```

1. **Configurar seu nome de usuário e e-mail**:

Antes de fazer commits, é uma boa prática configurar o seu nome de usuário e e-mail no Git.

```
git config --global user.name "Seu Nome"
git config --global user.email "seu.email@example.com"
```

Substitua "Seu Nome" pelo seu nome e "[seu.email@example.com](mailto:seu.email@example.com)" pelo seu endereço de e-mail.

1. **Adicionar arquivos ao repositório**:

Para começar a rastrear as alterações dos arquivos, você precisa adicioná-los ao repositório. Use o seguinte comando para adicionar todos os arquivos modificados e novos ao estágio de preparação (staging area).

```
git add .
```

Se você quiser adicionar apenas um arquivo específico, use:

```
git add nome_do_arquivo
```

1. **Realizar um commit**:

Depois de adicionar os arquivos ao estágio de preparação, você pode criar um commit. Os commits são usados para salvar suas alterações no histórico do repositório.

```
git commit -m "Mensagem do commit"
```

Substitua "Mensagem do commit" por uma descrição concisa do que foi feito no commit. Tente ser claro e descritivo para que seja fácil entender as alterações mais tarde.

1. **Verificar o status do repositório**:

Você pode verificar o status atual do repositório para ver quais arquivos foram modificados, quais estão no estágio de preparação e quais estão confirmados no último commit.

```
git status
```

1. **Visualizar o histórico de commits**:

Para ver o histórico de commits realizados no repositório, você pode usar o seguinte comando:

```
git log
```

Isso mostrará informações detalhadas sobre cada commit, incluindo o hash do commit, autor, data e a mensagem associada.

Esses são os comandos essenciais para começar a trabalhar com o Git no seu dia a dia. À medida que você avança no aprendizado, poderá explorar mais comandos e conceitos avançados, como branches, fusão (merge), resolução de conflitos, trabalhar com repositórios remotos, entre outros. O Git é uma ferramenta poderosa que pode aumentar significativamente a eficiência e organização do seu fluxo de trabalho no desenvolvimento de software. Lembre-se de praticar regularmente para se familiarizar com os comandos e conceitos.

### [IGNORANDO ARQUIVOS NO GIT | GIT MÃO NA MASSA](https://www.youtube.com/watch?v=Cm-biDiU2cc&list=PLMFPOLE2cW1xAlr3QOM8xV-IBFjzXnsqf&index=3)

O arquivo .gitignore é uma ferramenta importante no Git que permite ignorar certos tipos de arquivos ou diretórios, impedindo que eles sejam versionados no repositório. Isso é especialmente útil para evitar que arquivos desnecessários ou sensíveis sejam incluídos no histórico do projeto.

O arquivo .gitignore é colocado na raiz do repositório e contém uma lista de padrões que especificam quais arquivos e diretórios devem ser ignorados pelo Git. Cada linha do arquivo .gitignore representa um padrão que descreve quais arquivos ou pastas devem ser excluídos. Os padrões podem ser especificados de várias maneiras:

1. **Nome de arquivo exato**: Para ignorar um arquivo específico, basta escrever o nome do arquivo no arquivo .gitignore.

   Exemplo:

```
arquivo_para_ignorar.txt
```

**Wildcard (coringa)**: É possível usar coringas para combinar vários arquivos com o mesmo padrão.

Exemplo:

```lua
*.log            # Irá ignorar todos os arquivos com extensão .log
pasta_tmp/       # Irá ignorar a pasta chamada "pasta_tmp" e todo o seu conteúdo
```

**Comentários**: Linhas que começam com "#" são tratadas como comentários e não têm efeito no padrão.

Exemplo:

```
# Isso é um comentário
```

**Negar padrões**: Às vezes, você pode querer ignorar a maioria dos arquivos em um diretório, mas deseja rastrear um ou outro arquivo específico. Você pode negar padrões precedendo-os com um "!".

Exemplo:

1. ```lua
   *.log            # Irá ignorar todos os arquivos com extensão .log
   !important.log   # Mas não ignore o arquivo important.log
   ```

É importante notar que, se um arquivo já foi adicionado ao repositório antes de adicionar sua entrada no .gitignore, ele continuará a ser rastreado. Para deixar de rastrear o arquivo, você precisa removê-lo do repositório usando o seguinte comando:

```
git rm --cached caminho/do/arquivo
```

Depois de adicionar padrões ao arquivo .gitignore e salvar as alterações, o Git ignorará automaticamente os arquivos e diretórios correspondentes em operações futuras de adição e commit.

Usar o arquivo .gitignore é uma prática recomendada ao desenvolver projetos, pois ajuda a manter o repositório limpo, evita a inclusão de arquivos desnecessários e minimiza conflitos em colaborações com outros desenvolvedores.

### [JUNTANDO E DESFAZENDO COMMITS | GIT MÃO NA MASSA](https://www.youtube.com/watch?v=3P_PHz10t7g&list=PLMFPOLE2cW1xAlr3QOM8xV-IBFjzXnsqf&index=4)

Com certeza! Vamos falar sobre o comando `git branch`, o conceito de merge e como reverter ou apagar commits usando `git revert` e `git reset`.

1. **Comando `git branch`**:

O comando `git branch` é utilizado para listar, criar ou deletar branches (ramificações) no repositório Git. Branches são linhas de desenvolvimento independentes que permitem trabalhar em funcionalidades ou correções sem interferir diretamente no código principal (branch principal, normalmente chamada de "master" ou "main").

- Para listar todas as branches no repositório e destacar a branch atual:

```
git branch
```

Para criar uma nova branch:

```
git branch nome_da_branch
```

Para mudar para outra branch:

```
git checkout nome_da_branch
```

Para criar e mudar para uma nova branch em um único comando:

```
git checkout -b nome_da_branch
```

Para deletar uma branch (só é possível deletar branches que não estejam em uso):

- ```
  git branch -d nome_da_branch
  ```

1. **Conceito de merge**:

O merge (fusão) é o processo de combinar as alterações feitas em uma branch específica com outra branch, geralmente a branch principal (master ou main). Isso permite incorporar as modificações de uma funcionalidade ou correção em desenvolvimento de volta à linha principal de desenvolvimento.

- Para realizar o merge de uma branch específica à branch atual:

- ```
  git merge nome_da_branch
  ```

  Ao executar esse comando, o Git tentará combinar as alterações da branch `nome_da_branch` na branch em que você está no momento.

1. **Reverter ou apagar commits usando `git revert` e `git reset`**:

- **`git revert`**: O comando `git revert` é usado para criar um novo commit que desfaz as alterações introduzidas em um commit específico. Ele não altera o histórico do projeto, mas cria um novo commit que "desfaz" o commit selecionado.

- ```
  git revert <hash_do_commit>
  ```

  O `hash_do_commit` é o identificador único do commit que você deseja reverter.

- **`git reset`**: O comando `git reset` é mais poderoso e pode ser usado para desfazer alterações de commits de diferentes formas.

  - `git reset --soft <hash_do_commit>`: Mantém as alterações do commit selecionado no diretório de trabalho. Você pode fazer novos commits para reverter as alterações se necessário.
  - `git reset --mixed <hash_do_commit>`: Remove as alterações do commit selecionado do índice (área de staging). As alterações permanecem no diretório de trabalho, e você pode adicionar ou descartá-las antes de fazer um novo commit.
  - `git reset --hard <hash_do_commit>`: Descarta todas as alterações feitas no commit selecionado. Tenha cuidado ao usar esse comando, pois ele apaga permanentemente as alterações.

  É importante mencionar que, ao usar `git reset`, o histórico do projeto é alterado, e os commits após o commit selecionado podem ser perdidos. Portanto, é recomendado usá-lo com cuidado.

Com esses comandos, você pode gerenciar efetivamente as branches, combinar alterações entre elas e reverter ou apagar commits, permitindo um fluxo de trabalho mais eficiente e seguro no Git. Lembre-se de sempre verificar o status do repositório antes de realizar qualquer operação importante e de fazer backup das suas alterações importantes antes de usar comandos que alterem o histórico do projeto.

### [REPOSITÓRIOS REMOTOS NO GIT | GIT MÃO NA MASSA](https://www.youtube.com/watch?v=AukXdcB2Rt8&list=PLMFPOLE2cW1xAlr3QOM8xV-IBFjzXnsqf&index=5)

Trabalhar com repositórios remotos é uma parte essencial do uso do Git, especialmente quando se colabora com outros desenvolvedores ou quando você deseja fazer backup do seu código em um servidor centralizado. Abaixo estão os principais comandos para trabalhar com repositórios remotos:

1. **`git remote`**:

O comando `git remote` permite gerenciar os repositórios remotos associados ao seu projeto.

- Para listar os repositórios remotos configurados no seu projeto:

```
git remote -v
```

Para adicionar um novo repositório remoto:

- ```
  git remote add nome_remoto URL_do_repositorio
  ```

  Substitua `nome_remoto` pelo nome que você deseja dar ao repositório remoto (por exemplo, "origin"), e `URL_do_repositorio` pela URL do repositório remoto (por exemplo, um repositório no GitHub).

1. **`git push`**:

O comando `git push` é usado para enviar os commits do seu repositório local para um repositório remoto.

- Para enviar os commits da branch atual para o repositório remoto:

```
git push nome_remoto nome_branch
```

Substitua `nome_remoto` pelo nome do repositório remoto configurado (por exemplo, "origin") e `nome_branch` pelo nome da branch que você deseja enviar.

Se você quiser enviar os commits para uma branch remota com o mesmo nome da branch local:

```
git push nome_remoto nome_branch
```

Para enviar os commits de todas as branches locais para o repositório remoto:

- ```
  git push --all nome_remoto
  ```

1. **`git pull`**:

O comando `git pull` é usado para obter as alterações do repositório remoto e atualizar seu repositório local.

- Para obter e aplicar as alterações da branch remota para a branch atual:

```
git pull nome_remoto nome_branch
```

Substitua `nome_remoto` pelo nome do repositório remoto configurado (por exemplo, "origin") e `nome_branch` pelo nome da branch que você deseja atualizar.

Se você quiser atualizar a branch local com o mesmo nome da branch remota:

- ```
  git pull nome_remoto nome_branch
  ```

1. **`git clone`**:

O comando `git clone` é usado para criar uma cópia local de um repositório remoto completo.

- Para clonar um repositório remoto:

- ```
  git clone URL_do_repositorio
  ```

  Substitua `URL_do_repositorio` pela URL do repositório remoto que você deseja clonar.

Esses são os principais comandos para trabalhar com repositórios remotos no Git. Eles permitem que você compartilhe e sincronize suas alterações com outros colaboradores, além de possibilitar o trabalho em diferentes máquinas de forma eficiente. Lembre-se de sempre verificar o status do repositório antes de executar comandos de push e pull para evitar conflitos indesejados.
